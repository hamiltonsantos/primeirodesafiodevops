FROM debian:latest

# Atualiza a imagem com os pacotes
RUN apt-get update && apt-get upgrade -y

# Instala o NGINX para testar
RUN apt-get install nginx -y

# Exporta a porta 8000
EXPOSE 80

# Comando para iniciar ( NGINX ) no Container
CMD ["nginx", "-g", "daemon off;"]